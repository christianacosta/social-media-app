import React, { Component } from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import CardMedia from "@material-ui/core/CardMedia";
import { Link } from "react-router-dom";

const styles = {
  card: {
    display: "flex",
    marginBottom: 20
  },
  image: {
    minWidth: 100,
    minHeight: 50
  },
  content: {
    padding: 25
  }
};

export class Message extends Component {
  render() {
    const {
      classes,
      message: { body, createdAt, userHandle, userImage }
    } = this.props;
    return (
      <Card className={classes.card}>
        <CardMedia
          className={classes.image}
          image={userImage}
          title="Profile Image"
        />
        <CardContent className="classes.content">
          <Typography
            component={Link}
            to={`/users/${userHandle}`}
            color={"primary"}
            variant="h5"
          >
            {userHandle}
          </Typography>
          <Typography color="textSecondary">{createdAt}</Typography>
          <Typography>{body}</Typography>
        </CardContent>
      </Card>
    );
  }
}

export default withStyles(styles)(Message);
