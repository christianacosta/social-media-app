import React, { Component } from "react";
import { Grid } from "@material-ui/core";
import axios from "axios";
import Message from "../components/Message";
export class home extends Component {
  state = {
    messages: null
  };
  componentDidMount() {
    axios
      .get("/messages")
      .then(res => {
        this.setState({
          messages: res.data
        });
      })
      .catch(err => console.log(err));
  }
  render() {
    let recentMessagesMarkup = this.state.messages ? (
      this.state.messages.map(message => (
        <Message key={message.messageId} message={message} />
      ))
    ) : (
      <p>Loading...</p>
    );
    return (
      <Grid container spacing={2}>
        <Grid item sm={8} xs={12}>
          {recentMessagesMarkup}
        </Grid>

        <Grid item sm={4} xs={12}>
          <p>Profile...</p>
        </Grid>
      </Grid>
    );
  }
}

export default home;
