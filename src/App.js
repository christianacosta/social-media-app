import React from "react";
import "./App.css";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import createMuiTheme from "@material-ui/core/styles/createMuiTheme";
import { ThemeProvider as MuiThemeProvider } from "@material-ui/core/styles";
//Pages
import home from "./pages/home";
import login from "./pages/login";
import signup from "./pages/signup";

//Components
//import Navbar from "./components/Navbar";
import { Container } from "@material-ui/core";

import axios from "axios";

if (process.env.NODE_ENV) {
  axios.defaults.baseURL = "http://localhost:5000/socialape-a5c69/us-east1/api";
} else {
  axios.defaults.baseURL =
    "https://us-east1-socialape-a5c69.cloudfunctions.net/api";
}
const theme = createMuiTheme({
  palette: {
    primary: {
      light: "33c9dc",
      main: "#00bcd4",
      dark: "#008394",
      contrastText: "#fff"
    },
    secondary: {
      light: "#ff6333",
      main: "#ff3d00",
      dark: "#b22a00",
      contrastText: "#fff"
    }
  }
});

function App() {
  return (
    <MuiThemeProvider theme={theme}>
      <div className="App">
        <Router>
          <Container>
            <Switch>
              <Route exact path="/" component={home} />
              <Route path="/login" component={login} />
              <Route path="/signup" component={signup} />
            </Switch>
          </Container>
        </Router>
      </div>
    </MuiThemeProvider>
  );
}

export default App;
